#include <iostream>

using namespace std;

struct Data {
    int value;
    string str;
};

int main(int argc, char **argv) {
    unsigned counter = 0;
    Data temp;
    Data *data = NULL;
    Data *newNote;

    cout << "Enter a note\n"
              << "Value: ";
    cin >> temp.value;
    cout << "String: ";
    cin >> temp.str;
    cout << temp.str <<endl;
    cout << "\n";

    newNote = (Data*) realloc(data, (counter + 1) * sizeof(Data));
    if (newNote == NULL) {
        free(newNote);
        cout << "Error!" <<endl;
        exit(1);
    }
    data = newNote;
    data[counter].value = temp.value;
    data[counter].str = temp.str;
    counter++;

    int choice = 0;
    while (choice != 3) {
        cout << "What do you want to do?\n"
                  << "1) Add a note\n"
                  << "2) Print all notes\n"
                  << "3) Quit the program\n"
                  << "Your choice: ";
        cin >> choice;
        cout << "\n";
        switch (choice) {
            case 1:
                cout << "Entrer a note\n"
                     << "Value: ";
                cin >> temp.value;
                cout << "String: ";
                cin >> temp.str;
                cout << "\n";

                newNote = (Data*) realloc(data, sizeof(counter + 1) * sizeof(Data));
                if (newNote == NULL) {
                    free(newNote);
                    cout << "Error!" <<endl;
                    exit(1);
                }
                data = newNote;
                data[counter].value = temp.value;
                data[counter].str = temp.str;
                counter++;
                break;
            case 2:
                for (unsigned count = 0; count < counter; ++count) {
                    cout << count + 1 << ")\n"
                              << "   Value: " << data[count].value << "\n"
                              << "   String: " << data[count].str << std::endl;
                }
                cout << "\n";
                break;
            case 3:
                cout << "Exit...\n";
                return 0;
            default:
                cout << "Invalid input!\n";
        }
    }
}